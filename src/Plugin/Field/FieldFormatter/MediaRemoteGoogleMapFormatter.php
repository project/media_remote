<?php

namespace Drupal\media_remote\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'media_remote_google_map' formatter.
 *
 * Google documentation:
 * https://support.google.com/mymaps/answer/3024454
 *
 * @FieldFormatter(
 *   id = "media_remote_google_map",
 *   label = @Translation("Remote Media - Google Map"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class MediaRemoteGoogleMapFormatter extends MediaRemoteFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getUrlRegexPattern() {
    return '/^https:\/\/www\.google\.com\/maps\/d(\/u\/\d)?\/(edit|viewer|embed)\?mid=(\w+)/';
  }

  /**
   * {@inheritdoc}
   */
  public static function getValidUrlExampleStrings(): array {
    return [
      'https://www.google.com/maps/d/edit?mid=[your-map-hash]',
      'https://www.google.com/maps/d/viewer?mid=[your-map-hash]',
      'https://www.google.com/maps/d/u/0/embed?mid=[your-map-hash]',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function deriveMediaDefaultNameFromUrl($url) {
    $matches = [];
    $pattern = static::getUrlRegexPattern();
    if (preg_match($pattern, $url, $matches)) {
      return t('Google map from @url', [
        '@url' => self::getEmbedUrl($url),
      ]);
    }
    return parent::deriveMediaDefaultNameFromUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      if ($item->isEmpty()) {
        continue;
      }
      $elements[$delta] = [
        '#theme' => 'media_remote_google_map',
        '#title' => $item->getEntity()->label() ?? 'Google My Maps embed',
        '#url' => self::getEmbedUrl($item->value),
        '#width' => $this->getSetting('width') ?? 960,
        '#height' => $this->getSetting('height') ?? 600,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'width' => 960,
        'height' => 600,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
        'width' => [
          '#type' => 'number',
          '#title' => $this->t('Width'),
          '#default_value' => $this->getSetting('width'),
          '#size' => 5,
          '#maxlength' => 5,
          '#field_suffix' => $this->t('pixels'),
          '#min' => 50,
        ],
        'height' => [
          '#type' => 'number',
          '#title' => $this->t('Height'),
          '#default_value' => $this->getSetting('height'),
          '#size' => 5,
          '#maxlength' => 5,
          '#field_suffix' => $this->t('pixels'),
          '#min' => 50,
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Iframe size: %width x %height pixels', [
      '%width' => $this->getSetting('width'),
      '%height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

  /**
   * Converts the user provided url into a valid embedding url.
   *
   * @param string $url
   *   Initial url.
   *
   * @return string
   *   A url valid for embedding.
   */
  protected static function getEmbedUrl(string $url): string {
    $pattern = static::getUrlRegexPattern();
    if (preg_match($pattern, $url, $matches)) {
      // Remove "/u/0" if present.
      if (!empty($matches[1])) {
        $url = str_replace($matches[1], '', $url);
      }
      // If it's "edit" url or "viewer" url, replace with embed.
      $url = str_replace($matches[2], 'embed', $url);
    }
    return $url;
  }

}
